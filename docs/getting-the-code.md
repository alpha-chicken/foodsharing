# Getting the code

We use the version control system git.
The code is hosted on [gitlab](https://gitlab.com/foodsharing-dev/foodsharing).
To get the code and contribute create an account and apply for code access and tell a bit about you on the [Slack](https://slackin.yunity.org/) channel.

For Git, we recommend to use SSH (and the following documentation is supposing that). See e.g. [this documenation](https://docs.gitlab.com/ce/ssh/README.html) if you need to configure GitLab for this.

For first use of git search for tutorials, there are a lot good ones out there.
